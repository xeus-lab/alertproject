package xyz.stepsecret.alertproject;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class AssignAdapter extends RecyclerView.Adapter<AssignAdapter.MyViewHolder> {

    private List<Assign> assignList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView detail_name_doctor, department_doc, fullname_Patient;
        public TextView phone_number, detail, behave;
        public TextView datetime, times;

        public MyViewHolder(View view) {
            super(view);
            detail_name_doctor = (TextView) view.findViewById(R.id.detail_name_doctor);
            department_doc = (TextView) view.findViewById(R.id.department_doc);

            fullname_Patient = (TextView) view.findViewById(R.id.fullname_Patient);
            phone_number = (TextView) view.findViewById(R.id.phone_number);
            detail = (TextView) view.findViewById(R.id.detail);
            behave = (TextView) view.findViewById(R.id.behave);
            datetime = (TextView) view.findViewById(R.id.datetime);
            times = (TextView) view.findViewById(R.id.times);
        }
    }


    public AssignAdapter(List<Assign> assignList) {
        this.assignList = assignList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Assign movie = assignList.get(position);
        holder.detail_name_doctor.setText(movie.getDetail_name_doctor());
        holder.department_doc.setText(movie.getDepartment_doctor());

        holder.fullname_Patient.setText(movie.getFullname_Patient());
        holder.phone_number.setText(movie.getPhone_number());
        holder.detail.setText(movie.getDetail());
        holder.behave.setText(movie.getBehave());
        holder.datetime.setText(movie.getDatetime());
        holder.times.setText(movie.getTimes());
    }

    @Override
    public int getItemCount() {
        return assignList.size();
    }
}
