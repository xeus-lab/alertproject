package xyz.stepsecret.alertproject.API;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.PUT;
import xyz.stepsecret.alertproject.Model.PutTOKEN_Model;

/**
 * Created by stepsecret on 15/2/2559.
 */
public interface PutTOKEN_API {

    @FormUrlEncoded
    @POST("/alert/api/v2/saveToken")
    public void PutTOKEN(@Field("identification_number") String identification_number,@Field("token") String token,Callback<PutTOKEN_Model> response);
}
