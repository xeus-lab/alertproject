package xyz.stepsecret.alertproject.API;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;
import xyz.stepsecret.alertproject.Model.Get_Assign_Model;

/**
 * Created by Mylove on 3/1/2559.
 */
public interface Get_Assign_API {

    @GET("/alert/api/v2/get_Assign")
    public void Get_Assign(
            @Query("input_search") String input_search,
            Callback<Get_Assign_Model> response);
}
