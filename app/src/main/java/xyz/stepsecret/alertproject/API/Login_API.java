package xyz.stepsecret.alertproject.API;

/**
 * Created by Assanee on 8/7/2558.
 */
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

import retrofit.Callback;
import xyz.stepsecret.alertproject.Model.Login_Model;


public interface Login_API {

    @FormUrlEncoded
    @POST("/alert/api/v2/login")
    public void SignIN(@Field("identification_number") String identification_number, @Field("password") String password, Callback<Login_Model> response);


}
