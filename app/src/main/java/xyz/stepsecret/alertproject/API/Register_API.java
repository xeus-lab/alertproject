package xyz.stepsecret.alertproject.API;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import xyz.stepsecret.alertproject.Model.Register_Model;

/**
 * Created by Assanee on 8/7/2558.
 */
public interface Register_API {

    @FormUrlEncoded
    @POST("/alert/api/v2/register")
    public void SignUP(@Field("identification_number") String identification_number, @Field("first_name") String first_name
            , @Field("last_name") String last_name, @Field("phone_number") String phone_number, @Field("gender") String gender
            , @Field("location") String location, @Field("province") String province, @Field("zip_code") String zip_code
            , @Field("email") String email, @Field("password") String password, @Field("password_again") String password_again
            , @Field("class_data") String class_data
            , Callback<Register_Model> response);

}
