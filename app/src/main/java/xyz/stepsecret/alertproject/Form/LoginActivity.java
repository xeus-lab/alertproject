package xyz.stepsecret.alertproject.Form;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import xyz.stepsecret.alertproject.API.Login_API;
import xyz.stepsecret.alertproject.ConfigAPI;
import xyz.stepsecret.alertproject.MainActivity;
import xyz.stepsecret.alertproject.Model.Login_Model;
import xyz.stepsecret.alertproject.R;
import xyz.stepsecret.alertproject.TinyDB.TinyDB;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;

    private RestAdapter restAdapter;

    private TinyDB Store_data;

    EditText identification_number;
    EditText _password;

    Button _loginButton;
   TextView _signupLink;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Store_data = new TinyDB(getApplicationContext());

        Check_login();

        _loginButton = (Button) findViewById(R.id.btn_login);
        _signupLink = (TextView) findViewById(R.id.link_signup);
        identification_number = (EditText) findViewById(R.id.input_email);
        _password = (EditText) findViewById(R.id.input_password);

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });

        _signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
            }
        });


    }

    public void Check_login()
    {
        Boolean login = Store_data.getBoolean("login", false);

        if(login==true)
        {
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
        }
    }

    public void login() {
        Log.d(TAG, "Login");

       /* if (!validate()) {
            onLoginFailed();
            return;
        }
*/


        String identification = identification_number.getText().toString();
        String password = _password.getText().toString();

        // TODO: Implement your own authentication logic here.


        Call_login(identification,password);
    }

    public void Call_login(String identification,String password)
    {
        restAdapter = new RestAdapter.Builder()
                .setEndpoint(ConfigAPI.API).build();

        final Login_API login_api = restAdapter.create(Login_API.class);

        login_api.SignIN(identification, password, new Callback<Login_Model>() {
            @Override
            public void success(Login_Model result, Response response) {

                if(!result.getError())
                {
                    Log.e(" TAG ","success");
                    Log.e(" TAG ",result.getIdentificationnumber());
                    Log.e(" TAG ",result.getFirstname());
                    Log.e(" TAG ",result.getLastname());
                    Log.e(" TAG ",result.getPhonenumber());
                    Log.e(" TAG ",result.getGender());
                    Log.e(" TAG ",result.getLocation());
                    Log.e(" TAG ",result.getProvince());
                    Log.e(" TAG ",result.getZipcode());
                    Log.e(" TAG ",result.getEmail());
                    Log.e(" TAG ",result.getApikey());
                    Log.e(" TAG ",result.getClassdata());

                    Store_data.putBoolean("login", true);
                    Store_data.putString("identificationnumber", result.getIdentificationnumber());
                    Store_data.putString("firstname", result.getFirstname());
                    Store_data.putString("lastname", result.getLastname());
                    Store_data.putString("phonenumber", result.getPhonenumber());
                    Store_data.putString("gender", result.getGender());
                    Store_data.putString("location", result.getLocation());
                    Store_data.putString("province", result.getProvince());
                    Store_data.putString("zipcode", result.getZipcode());
                    Store_data.putString("email", result.getEmail());
                    Store_data.putString("apikey", result.getApikey());
                    Store_data.putString("classdata", result.getClassdata());

                    Check_login();
                }
                else
                {
                    show_failure(result.getMessage());
                    Log.e(" TAG ","error");
                }



            }

            @Override
            public void failure(RetrofitError error) {

                show_failure(error.getMessage());
                Log.e(" TAG ","failure"+error.getMessage());

            }
        });
    }

    public void show_failure(String message)
    {
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(message)
                .show();
    }
    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {
        _loginButton.setEnabled(true);
        finish();
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

        _loginButton.setEnabled(true);
    }

   /* public boolean validate() {
        boolean valid = true;

        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }*/
}
