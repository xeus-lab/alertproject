package xyz.stepsecret.alertproject.Form;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import xyz.stepsecret.alertproject.API.Register_API;
import xyz.stepsecret.alertproject.ConfigAPI;
import xyz.stepsecret.alertproject.Model.Register_Model;
import xyz.stepsecret.alertproject.R;

public class RegisterActivity extends AppCompatActivity {
    private static final String TAG = "RegisterActivity";

    private String store_gender = "";
    private String store_province = "";

    private RestAdapter restAdapter;

    EditText identification_number;
    EditText first_name;
    EditText last_name;
    EditText phone_number;
    EditText location;
    EditText zip_code;
    EditText email;
    EditText password;
    EditText password_again;
    Spinner gender;
    Spinner province;


    Button _signupButton;

    TextView _loginLink;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        identification_number = (EditText) findViewById(R.id.identification_number);
        first_name = (EditText) findViewById(R.id.first_name);
        last_name = (EditText) findViewById(R.id.last_name);
        phone_number = (EditText) findViewById(R.id.phone_number);
        location = (EditText) findViewById(R.id.location);
        zip_code = (EditText) findViewById(R.id.zip_code);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        password_again = (EditText) findViewById(R.id.repassword);

        gender = (Spinner) findViewById(R.id.input_gender);
        province = (Spinner) findViewById(R.id.province) ;

        _signupButton = (Button) findViewById(R.id.btn_signup);

        _loginLink = (TextView) findViewById(R.id.link_login);

        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        _loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                finish();
            }
        });



        gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {

                store_gender = adapter.getItemAtPosition(position).toString();


            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

        province.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {

                store_province = adapter.getItemAtPosition(position).toString();


            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

    }

    public void signup() {
        Log.d(TAG, "Signup");



        identification_number = (EditText) findViewById(R.id.identification_number);
        first_name = (EditText) findViewById(R.id.first_name);
        last_name = (EditText) findViewById(R.id.last_name);
        phone_number = (EditText) findViewById(R.id.phone_number);
        location = (EditText) findViewById(R.id.location);
        zip_code = (EditText) findViewById(R.id.zip_code);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        password_again = (EditText) findViewById(R.id.repassword);



        Call_Signup(identification_number.getText().toString(), first_name.getText().toString(), last_name.getText().toString()
                , phone_number.getText().toString(), store_gender, location.getText().toString(), store_province, zip_code.getText().toString()
                , email.getText().toString(), password.getText().toString(),
                password_again.getText().toString(), "User");


    }

    public void  Call_Signup(String identification_number1,String first_name1,String last_name1,String phone_number1,String store_gender1
            ,String location1,String store_province1,String zip_code1,String email1,String password1,String password_again1,String class_data)
    {
        restAdapter = new RestAdapter.Builder()
                .setEndpoint(ConfigAPI.API).build();

        final Register_API signup_api = restAdapter.create(Register_API.class);

        signup_api.SignUP( identification_number1, first_name1, last_name1, phone_number1, store_gender1
                , location1, store_province1, zip_code1, email1, password1, password_again1, class_data, new Callback<Register_Model>() {
            @Override
            public void success(Register_Model result, Response response) {

                if(!result.getError())
                {

                    show_success(result.getMessage());
                }
                else
                {
                    show_failure(result.getMessage());
                }

            }

            @Override
            public void failure(RetrofitError error) {

                Log.e(" TAG ","failure "+error.getMessage());
                show_failure(error.getMessage());
            }
        });
    }

    public void show_success(String message)
    {
        new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                .setTitleText(message)
                .setContentText("Close to login")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        sDialog.dismiss();
                        finish();
                    }
                })
                .show();
    }

    public void show_failure(String message)
    {
        new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
               .setTitleText(message)
                .show();
    }


    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();

    }

   /* public boolean validate(String username, String firstname, String lastname, String birthday,
                            String email, String password, String repassword) {
        boolean valid = true;



        if (username.isEmpty() || username.length() < 3) {
            _username.setError("at least 3 characters");
            valid = false;
        } else {
            _username.setError(null);
        }

        if (firstname.isEmpty() || firstname.length() < 3) {
            _firstname.setError("at least 3 characters");
            valid = false;
        } else {
            _firstname.setError(null);
        }

        if (lastname.isEmpty() || lastname.length() < 3) {
            _lastname.setError("at least 3 characters");
            valid = false;
        } else {
            _lastname.setError(null);
        }

        String[] parts = birthday.split("-");
        if( birthday.isEmpty() || parts[0].length() != 4)
        {
            _birthday.setError("year is failure");
        }
        else if(parts[1].length() < 1 || parts[1].length() > 2)
        {
            _birthday.setError("month is failure");
        }
        else if(parts[2].length() < 1 || parts[2].length() > 2 )
        {
            _birthday.setError("day is failure");
        }
        else
        {
            _birthday.setError(null);
        }




        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            _passwordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        if (repassword.isEmpty() || repassword.length() < 4 || repassword.length() > 10) {
            _repasswordText.setError("between 4 and 10 alphanumeric characters");
            valid = false;
        } else {
            _repasswordText.setError(null);
        }

        if(!password.equals(repassword))
        {
            _passwordText.setError("password not macth");
            _repasswordText.setError("password not macth");
        }
        return valid;
    }*/
}