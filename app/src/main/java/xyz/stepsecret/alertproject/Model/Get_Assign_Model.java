package xyz.stepsecret.alertproject.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Mylove on 3/1/2559.
 */
public class Get_Assign_Model {

    @SerializedName("data")
    private String[][] data ;

    @SerializedName("error")
    private Boolean error ;

    @SerializedName("message")
    private String message ;


    public String[][] getData() {
        return data;
    }

    public Boolean getError() {
        return error;
    }

    public String getMessage() {
        return message;
    }




}
