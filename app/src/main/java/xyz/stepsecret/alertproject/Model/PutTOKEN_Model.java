package xyz.stepsecret.alertproject.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by stepsecret on 15/2/2559.
 */
public class PutTOKEN_Model {

    @SerializedName("error")
    private Boolean error ;

     @SerializedName("message")
     private String message ;



    public Boolean getError() {
        return error;
    }
    public String getMessage() {
        return message;
    }


}
