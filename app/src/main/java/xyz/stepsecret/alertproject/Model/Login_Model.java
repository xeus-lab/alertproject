package xyz.stepsecret.alertproject.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Assanee on 8/7/2558.
 */
public class Login_Model {

    @SerializedName("error")
    private Boolean error ;

    @SerializedName("identificationnumber")
    private String identificationnumber ;

    @SerializedName("firstname")
    private String firstname;

    @SerializedName("lastname")
    private String lastname;

    @SerializedName("phonenumber")
    private String phonenumber;

    @SerializedName("gender")
    private String gender;

    @SerializedName("location")
    private String location;

    @SerializedName("province")
    private String province;

    @SerializedName("zipcode")
    private String zipcode;

    @SerializedName("email")
    private String email;

    @SerializedName("classdata")
    private String classdata;

    @SerializedName("apikey")
    private String apikey;

    @SerializedName("createdAt")
    private String createdAt;

    @SerializedName("message")
    private String message;


    public Boolean getError() {
        return error;
    }

    public String getIdentificationnumber() {
        return identificationnumber;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public String getGender() {
        return gender;
    }

    public String getLocation() {
        return location;
    }

    public String getProvince() {
        return province;
    }

    public String getZipcode() {
        return zipcode;
    }

    public String getEmail() {
        return email;
    }

    public String getApikey() {
        return apikey;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getClassdata() {
        return classdata;
    }

    public String getMessage() {
        return message;
    }

}
