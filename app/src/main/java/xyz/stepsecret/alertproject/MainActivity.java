package xyz.stepsecret.alertproject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import xyz.stepsecret.alertproject.API.Get_Assign_API;
import xyz.stepsecret.alertproject.API.PutTOKEN_API;
import xyz.stepsecret.alertproject.Form.LoginActivity;
import xyz.stepsecret.alertproject.GCM.QuickstartPreferences;
import xyz.stepsecret.alertproject.GCM.RegistrationIntentService;
import xyz.stepsecret.alertproject.Model.Get_Assign_Model;
import xyz.stepsecret.alertproject.Model.PutTOKEN_Model;
import xyz.stepsecret.alertproject.TinyDB.TinyDB;

public class MainActivity extends AppCompatActivity {

    public static RestAdapter restAdapter;

    private TinyDB Store_data;

    private static final String TAG = "MainActivity";
    private Calendar currentCalender = Calendar.getInstance(Locale.getDefault());
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());

    

    private List<Assign> assignList = new ArrayList<>();
    private RecyclerView recyclerView;
    private AssignAdapter mAdapter;
    SwipeRefreshLayout mSwipeRefreshLayout;

    CompactCalendarView compactCalendarView;

    private String[][] StoreAssign;

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isReceiverRegistered;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        restAdapter = new RestAdapter.Builder()
                .setEndpoint(ConfigAPI.API).build();

        Store_data = new TinyDB(getApplicationContext());

        compactCalendarView = (CompactCalendarView) findViewById(R.id.compactcalendar_view);


        //set initial title
        getSupportActionBar().setTitle(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));

        //toolbar.setTitle(dateFormatForMonth.format(compactCalendarView.getFirstDayOfCurrentMonth()));

        findViewById(R.id.Logout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Confirm_Logout();
            }
        });


        // below allows you to configure color for the current day in the month
        compactCalendarView.setCurrentDayBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.black_semi_transparent));
        // below allows you to configure colors for the current day the user has selected
        compactCalendarView.setCurrentSelectedDayBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.rb_disable));

        //loadEvents(compactCalendarView);
        compactCalendarView.invalidate();


        compactCalendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {

                clearData(assignList,mAdapter,recyclerView);

                Assign movie;

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                for(int i = 0 ; i < StoreAssign.length ; i++)
                {

                    String someDate = StoreAssign[i][1];
                    Date date = null;
                    try {
                        date = sdf.parse(someDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    long timestamp = date.getTime();

                    if(timestamp == dateClicked.getTime())
                    {
                        String temp1 = "แพทย์ผู้นัด : "+StoreAssign[i][5]+StoreAssign[i][6]+" "+StoreAssign[i][7];
                        String temp2 = "แผนก : "+StoreAssign[i][8];

                        String temp3 = "ชื่อ : "+StoreAssign[i][9]+" "+StoreAssign[i][10];
                        String temp4 = "เบอร์โทรติดต่อ : "+StoreAssign[i][11];
                        String temp5 = "รายละเอียดของอาการ : "+StoreAssign[i][3];
                        String temp6 = "การประพฤติตน : "+StoreAssign[i][4];
                        String temp7 = "วันนัดตรวจ : "+StoreAssign[i][1];
                        String temp8 = "เวลา : "+StoreAssign[i][2];


                        movie = new Assign(temp1, temp2, temp3, temp4 ,temp5, temp6, temp7, temp8);
                        assignList.add(movie);
                    }

                }

                mAdapter.notifyDataSetChanged();

            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {

                getSupportActionBar().setTitle(dateFormatForMonth.format(firstDayOfNewMonth));

            }
        });


        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swifeRefresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.holo_green, R.color.holo_orange, R.color.holo_red);


        mAdapter = new AssignAdapter(assignList);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Log.e("TAG",""+position);
                Assign movie = assignList.get(position);
                //Toast.makeText(getApplicationContext(), movie.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();

                //assignList.clear();
                //recyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool());
            }

            @Override
            public void onLongClick(View view, int position) {

                Log.e("TAG",""+position);
                Assign movie = assignList.get(position);
                //Toast.makeText(getApplicationContext(), movie.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
            }
        }));

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

               // prepareMovieData();
                clearData(assignList,mAdapter,recyclerView);
                getAssign();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });


        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences
                        .getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                   // mInformationTextView.setText(getString(R.string.gcm_send_message));
                } else {
                   // mInformationTextView.setText(getString(R.string.token_error_message));
                }
            }
        };
        // Registering BroadcastReceiver
        registerReceiver();

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }


        getAssign();

    }
    public void clearData(List<Assign> mylist, AssignAdapter mAdapter, RecyclerView recyclerView) {
        mylist.removeAll(mylist);
        mAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(mAdapter);
    }


    void getAssign()
    {
        String ApiKey = Store_data.getString("identificationnumber");


        final Get_Assign_API get_assign = restAdapter.create(Get_Assign_API.class);

        get_assign.Get_Assign(ApiKey, new Callback<Get_Assign_Model>() {
                    @Override
                    public void success(Get_Assign_Model result, Response response) {

                        StoreAssign = result.getData();

                        Log.e("TAG","1 "+result.getError());
                        Log.e("TAG","2 "+result.getMessage());
                        Log.e("TAG","3 "+result.getData().length);

                        Assign movie;

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                       // System.out.println(date.getTime());

                        if(!result.getError())
                        {
                            for(int i = 0 ; i < result.getData().length ; i++)
                            {

                                String someDate = result.getData()[i][1]+" "+result.getData()[i][2];
                                Date date = null;
                                try {
                                    date = sdf.parse(someDate);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                long timestamp = date.getTime();
                                //Log.e("TAG",someDate+" >>> "+timestamp+" -> "+timestamp);


                                Event ev1 = new Event(Color.GREEN, timestamp, result.getData()[i][3]);
                                compactCalendarView.addEvent(ev1,true);

                                String temp1 = "แพทย์ผู้นัด : "+result.getData()[i][5]+result.getData()[i][6]+" "+result.getData()[i][7];
                                String temp2 = "แผนก : "+result.getData()[i][8];

                                String temp3 = "ชื่อ : "+result.getData()[i][9]+" "+result.getData()[i][10];
                                String temp4 = "เบอร์โทรติดต่อ : "+result.getData()[i][11];
                                String temp5 = "รายละเอียดของอาการ : "+result.getData()[i][3];
                                String temp6 = "การประพฤติตน : "+result.getData()[i][4];
                                String temp7 = "วันนัดตรวจ : "+result.getData()[i][1];
                                String temp8 = "เวลา : "+result.getData()[i][2];


                                movie = new Assign(temp1, temp2, temp3, temp4 ,temp5, temp6, temp7, temp8);
                                assignList.add(movie);


                            }

                            mAdapter.notifyDataSetChanged();

                        }
                        else
                        {
                            Log.e("TAG",""+response);

                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {

                        Log.e(" TAG ","failure "+error.getMessage());
                        //show_failure(error.getMessage());
                    }
                });
    }


    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }



    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        isReceiverRegistered = false;
        super.onPause();
    }

    private void registerReceiver(){
        if(!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
            isReceiverRegistered = true;
        }
    }
    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    public void Confirm_Logout()
    {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Logout");
        dialog.setIcon(R.mipmap.ic_launcher);
        dialog.setCancelable(true);
        dialog.setMessage("Do you want to logout?");
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                Logout();
            }
        });

        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        dialog.show();
    }


    public void Logout()
    {
        String ApiKey = Store_data.getString("identificationnumber");

        restAdapter = new RestAdapter.Builder()
                .setEndpoint(ConfigAPI.API).build();

        PutTOKEN_API Puttoken = restAdapter.create(PutTOKEN_API.class);
        Puttoken.PutTOKEN(ApiKey, "empty", new Callback<PutTOKEN_Model>() {
            @Override
            public void success(PutTOKEN_Model PutTOKEN, Response response) {


                Log.e("LOG TAG", "Put Token error : " + PutTOKEN.getError());

                Store_data.clear();

                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false).apply();

                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
                finish();

            }

            @Override
            public void failure(RetrofitError error) {

                Log.e("LOG TAG", "Logout failure");

                Logout();

            }
        });




    }

}
