package xyz.stepsecret.alertproject;

/**
 * Created by Lincoln on 15/01/16.
 */
public class Assign {

    private String detail_name_doctor, department_doc, fullname_Patient;
    private String phone_number, detail, behave;
    private String datetime, times;


    public Assign() {
    }

    public Assign(String title, String department_doc, String fullname_Patient, String phone_number, String detail, String behave, String datetime, String times) {
        this.detail_name_doctor = title;
        this.department_doc = department_doc;
        this.fullname_Patient = fullname_Patient;
        this.phone_number = phone_number;
        this.detail = detail;
        this.behave = behave;
        this.datetime = datetime;
        this.times = times;
    }

    public String getDetail_name_doctor() {
        return detail_name_doctor;
    }

    public void setDetail_name_doctor(String name) {
        this.detail_name_doctor = name;
    }

    public String getDepartment_doctor() {
        return department_doc;
    }

    public void setDepartment_doc(String department_doc) {
        this.department_doc = department_doc;
    }


    public String getFullname_Patient() {
        return fullname_Patient;
    }

    public void setFullname_Patient(String fullname_Patient) {
        this.fullname_Patient = fullname_Patient;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getBehave() {
        return behave;
    }

    public void setBehave(String behave) {
        this.behave = behave;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }


    public String getTimes() {
        return times;
    }

    public void setTimes(String times) {
        this.times = times;
    }





}
